package fr.deeplearning.abstractfactory;

import lombok.Getter;

@Getter
public class BowlMv implements Utensil {
    private String type;

    public BowlMv() {
        this.type = "BOWL_MW";
    }
}
