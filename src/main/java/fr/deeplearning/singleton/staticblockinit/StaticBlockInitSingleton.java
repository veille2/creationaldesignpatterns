package fr.deeplearning.singleton.staticblockinit;

import lombok.NoArgsConstructor;

import static lombok.AccessLevel.PRIVATE;

@NoArgsConstructor(access = PRIVATE)
public class StaticBlockInitSingleton {

    private static StaticBlockInitSingleton INSTANCE;

    static {
        try {
            INSTANCE = new StaticBlockInitSingleton();
        } catch (Exception e) {
            throw new RuntimeException("Error creating singleton instance");
        }
    }

    public static StaticBlockInitSingleton getInstance() {
        return INSTANCE;
    }
}
