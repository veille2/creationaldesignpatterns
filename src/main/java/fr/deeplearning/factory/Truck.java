package fr.deeplearning.factory;

public class Truck implements Vehicle {

    private static final String TRUCK = "TRUCK";

    @Override
    public String getVehicle() {
        return TRUCK;
    }
}
