package fr.deeplearning.abstractfactory;

public class MicrowaveSafeFactory implements AbstractUtensilFactory{

    public Utensil getPlate() {
        return new Plate();
    }

    public Utensil getBowl() {
        return new Bowl();
    }

    public Utensil getCup() {
        return new Cup();
    }
}
