package fr.deeplearning.prototype;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class PrototypeTest {

    @Test
    void should_clone_itself() {
        var woodBuilding = new WoodBuilding(34, 55);

        var clonedWoodBuilding = woodBuilding.copy();

        assertEquals(woodBuilding.getHeight(), clonedWoodBuilding.getHeight());
        assertEquals(woodBuilding.getWidth(), clonedWoodBuilding.getWidth());
    }
}
