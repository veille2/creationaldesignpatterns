package fr.deeplearning.factory;

import org.junit.jupiter.api.Test;

import java.util.function.Supplier;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class VehicleFactoryTest {

    private static final String CAR = "CAR";
    private static final String TRUCK = "TRUCK";

    Supplier<VehicleFactoryLambda> vehicleFactoryLambda = VehicleFactoryLambda::new;

    @Test
    void should_create_car() {
        var vehicleFactory = new VehicleFactory();

        var car = vehicleFactory.getVehicle(CAR);

        assertEquals(car.getVehicle(), CAR);
    }

    @Test
    void should_create_truck() {
        var vehicleFactory = new VehicleFactory();

        var truck = vehicleFactory.getVehicle(TRUCK);

        assertEquals(truck.getVehicle(), TRUCK);
    }

    @Test
    void should_create_car_using_lambda_expressions() {

        var car = vehicleFactoryLambda.get().getVehicle(CAR);

        assertEquals(car.getVehicle(), CAR);
    }

    @Test
    void should_create_truck_using_lambda_expressions() {

        var truck = vehicleFactoryLambda.get().getVehicle(TRUCK);

        assertEquals(truck.getVehicle(), TRUCK);
    }
}
