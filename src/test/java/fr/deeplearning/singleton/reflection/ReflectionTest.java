package fr.deeplearning.singleton.reflection;

import fr.deeplearning.singleton.lazyinitialization.LazySingleton;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Constructor;

public class ReflectionTest {

    @Test
    public void should_create_new_instance() {

        var instanceOne = LazySingleton.getINTANCE();
        LazySingleton instanceTwo = null;

        try {
            Constructor[] constructors = LazySingleton.class.getDeclaredConstructors();
            for (Constructor constructor : constructors) {

                //Destroy the singleton pattern
                constructor.setAccessible(true);
                instanceTwo = (LazySingleton) constructor.newInstance();
                break;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        Assertions.assertNotEquals(instanceOne.hashCode(), instanceTwo.hashCode());
    }
}
