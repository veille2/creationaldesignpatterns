package fr.deeplearning.abstractfactory;

import lombok.Getter;

@Getter
public class Plate implements Utensil {

    private String type;

    public Plate(){
        this.type = "PLATE";
    }
}
