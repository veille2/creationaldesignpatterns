package fr.deeplearning.abstractfactory;

import lombok.Getter;

@Getter
public class CupMw implements Utensil {

    private String type;

    public CupMw() {
        this.type = "CUP_MW";
    }

}
