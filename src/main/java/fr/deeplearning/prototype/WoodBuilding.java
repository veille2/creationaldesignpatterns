package fr.deeplearning.prototype;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public class WoodBuilding extends Building {

    private double height;
    private double width;

    @Override
    public WoodBuilding copy() {
        return new WoodBuilding(this.height, this.width);

    }
}
