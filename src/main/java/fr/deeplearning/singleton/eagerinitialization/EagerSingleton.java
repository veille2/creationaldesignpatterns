package fr.deeplearning.singleton.eagerinitialization;

import lombok.NoArgsConstructor;

import static lombok.AccessLevel.PRIVATE;

@NoArgsConstructor(access = PRIVATE)
public class EagerSingleton {

    private static EagerSingleton INSTANCE = new EagerSingleton();

    public static EagerSingleton getINSTANCE() {
        return INSTANCE;
    }
}
