package fr.deeplearning.abstractfactory;

public interface AbstractUtensilFactory {

    Utensil getPlate();

    Utensil getBowl();

    Utensil getCup();
}
