package fr.deeplearning.singleton.lazyinitialization;

import lombok.NoArgsConstructor;

import static lombok.AccessLevel.PRIVATE;

@NoArgsConstructor(access = PRIVATE)
public class LazySingleton {

    private static LazySingleton INSTANCE;

    public static LazySingleton getINTANCE() {
        if (INSTANCE == null) {
            return new LazySingleton();
        }
        return INSTANCE;
    }
}
