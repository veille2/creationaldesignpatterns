package fr.deeplearning.abstractfactory;

import lombok.Getter;

@Getter
public class Cup implements Utensil {

    private String type;

    public Cup() {
        this.type = "CUP";
    }

}
