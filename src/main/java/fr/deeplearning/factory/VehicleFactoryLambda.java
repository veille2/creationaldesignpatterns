package fr.deeplearning.factory;

import java.util.Map;
import java.util.function.Supplier;

public class VehicleFactoryLambda {

    private static final String CAR = "CAR";
    private static final String TRUCK = "TRUCK";
    private static final Map<String, Supplier<Vehicle>> VEHICLES = Map.of(
            CAR, Car::new,
            TRUCK, Truck::new);

    public void addVehicle(String vehicleType, Supplier<Vehicle> vehicle) {
        VEHICLES.put(vehicleType, vehicle);
    }

    public Vehicle getVehicle(String vehicleType) {
        Supplier<Vehicle> vehicle = VEHICLES.get(vehicleType.toUpperCase());
        if (vehicle != null) {
            return vehicle.get();
        }
        throw new IllegalArgumentException("No such vehicle " + vehicleType.toUpperCase());
    }
}
