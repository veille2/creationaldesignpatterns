package fr.deeplearning.abstractfactory;

public class FactoryProducer {

    public static AbstractUtensilFactory getFactory(String choice){

        if("Microwave".equalsIgnoreCase(choice)){
            return new MicrowaveSafeFactory();
        }
        else if("Non-Microwave".equalsIgnoreCase(choice)){
            return new MicrowaveNonSafeFactory();
        }
        return null;
    }

}
