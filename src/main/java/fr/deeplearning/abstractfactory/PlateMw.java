package fr.deeplearning.abstractfactory;

import lombok.Getter;

@Getter
public class PlateMw implements Utensil {

    private String type;

    public PlateMw() {
        this.type = "PLATE_MW";
    }
}
