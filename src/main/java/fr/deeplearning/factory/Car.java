package fr.deeplearning.factory;

public class Car implements Vehicle {

    private static final String CAR = "CAR";

    @Override
    public String getVehicle() {
        return CAR;
    }
}
