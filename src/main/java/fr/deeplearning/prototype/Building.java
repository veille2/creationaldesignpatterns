package fr.deeplearning.prototype;

public abstract class Building {

    public abstract Building copy();
}
