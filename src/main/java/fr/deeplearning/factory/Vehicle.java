package fr.deeplearning.factory;

public interface Vehicle {
    String getVehicle();
}
