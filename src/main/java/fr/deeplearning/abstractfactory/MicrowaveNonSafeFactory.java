package fr.deeplearning.abstractfactory;

public class MicrowaveNonSafeFactory implements AbstractUtensilFactory{

    public Utensil getPlate() {
        return new PlateMw();
    }

    public Utensil getBowl() {
        return new BowlMv();
    }

    public Utensil getCup() {
        return new CupMw();
    }
}
