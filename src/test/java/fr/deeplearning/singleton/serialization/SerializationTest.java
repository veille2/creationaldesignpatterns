package fr.deeplearning.singleton.serialization;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.io.*;

public class SerializationTest {

    @Test
    public void should_create_new_instance_on_serialization() throws IOException, ClassNotFoundException {

        var instanceOne = SerializedSingleton.getInstance();
        var outputStream = new ObjectOutputStream(new FileOutputStream("serializeSingleton.ser"));

        outputStream.writeObject(instanceOne);
        outputStream.close();

        //deserialize from file to object

        var inputStream = new ObjectInputStream(new FileInputStream("serializeSingleton.ser"));
        var instanceTwo = (SerializedSingleton) inputStream.readObject();
        inputStream.close();

        Assertions.assertNotEquals(instanceOne.hashCode(), instanceTwo.hashCode());
    }
}
