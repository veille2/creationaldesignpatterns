package fr.deeplearning.factory;

public class VehicleFactory {

    private static final String CAR = "CAR";
    private static final String TRUCK = "TRUCK";


    public Vehicle getVehicle(String vehicleType) {

        if (CAR.equals(vehicleType)) {
            return new Car();
        }
        if (TRUCK.equals(vehicleType)) {
            return new Truck();
        }
        throw new IllegalArgumentException("No such vehicle " + vehicleType.toUpperCase());
    }
}
