package fr.deeplearning.abstractfactory;

import lombok.Getter;

@Getter
public class Bowl implements Utensil {

    private String type;

    public Bowl() {
        this.type = "BOWL";
    }
}
