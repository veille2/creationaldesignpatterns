package fr.deeplearning.builder;

import lombok.Getter;

@Getter
public class Developer {

    private final String firstName;
    private final String lastName;
    private final int age;
    private final String phone;
    private final String address;

    public Developer(DeveloperBuilder builder) {
        this.firstName = builder.firstName;
        this.lastName = builder.lastName;
        this.age = builder.age;
        this.phone = builder.phone;
        this.address = builder.address;
    }

    public static DeveloperBuilder builder() {
        return new DeveloperBuilder();
    }

    public static class DeveloperBuilder {
        private String firstName;
        private String lastName;
        private int age;
        private String phone;
        private String address;


        public DeveloperBuilder firstName(String firstName) {
            this.firstName = firstName;
            return this;
        }

        public DeveloperBuilder lastName(String lastName) {
            this.lastName = lastName;
            return this;
        }

        public DeveloperBuilder age(int age) {
            this.age = age;
            return this;
        }

        public DeveloperBuilder phone(String phone) {
            this.phone = phone;
            return this;
        }

        public DeveloperBuilder address(String address) {
            this.address = address;
            return this;
        }

        public Developer build() {
            return new Developer(this);
        }
    }
}
