package fr.deeplearning.abstractfactory;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class UtensilFactoryTest {

    @Test
    public void should_create_plate() {
        var utensilFactory = FactoryProducer.getFactory("Microwave");
        var plate = utensilFactory.getPlate();

        assertEquals(plate.getType(), "PLATE");
    }
}
