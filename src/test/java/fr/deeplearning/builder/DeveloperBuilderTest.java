package fr.deeplearning.builder;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class DeveloperBuilderTest {

    @Test
    public void should_create_new_developer_with_builder() {

        var developer = Developer.builder()
                                 .firstName("Lydia")
                                 .lastName("Dia")
                                 .age(29)
                                 .phone("+33678543421")
                                 .address("adress")
                                 .build();

        assertEquals(developer.getFirstName(), "Lydia");
    }
}
