package fr.deeplearning.abstractfactory;

public interface Utensil {
    String getType();
}
